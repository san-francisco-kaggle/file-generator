package sf_crime.address

import java.io.{File, PrintWriter}

import sf_crime._
import sf_crime.utils.{FileUtil, HasContext}

import scala.annotation.tailrec

trait OneHotFunctions extends HasContext with AddressConstants {

  def saveAsOneHotFiles(toSave: Seq[Address], filePrefix: String): Unit = {
    def getFileName(fileNumber: Int) = {
      s"$filePrefix${countLimit}_$fileNumber$csv"
    }

    val header = context.firstColumnHeader + internalDelimiter + "addresses..."
    val maxAddressIndex = getMaxAddressIndex(toSave)
    val (dealWith, rest) = toSave.splitAt(linesPerFile)

    saveSubFiles(dealWith, rest, getFileName, header, maxAddressIndex)
  }

  def saveAsMultipleHotFile(toSave: Seq[Address],fileName: String): Unit = {
    val header = context.firstColumnHeader + internalDelimiter + "indexes..."
    val maxAddressIndex = getMaxAddressIndex(toSave)
    val firstOneHotSize = 100
    val secondOneHotSize = ((maxAddressIndex + 1) / firstOneHotSize.toDouble).ceil.toInt

    val (dealWith, rest) = toSave.splitAt(linesPerFile)

    saveSubFiles(dealWith, rest, _ => fileName, header, firstOneHotSize, Some(secondOneHotSize))
  }

  private[this] def getMaxAddressIndex(addresses: Seq[Address]) = {
    addresses.maxBy(_.addressIdx.get).addressIdx.get
  }

  private[this] def saveSubFiles(toSave: Seq[Address],
                                 rest: Seq[Address],
                                 fileName: (Int) => String,
                                 header: String,
                                 firstOneHotSize: Int,
                                 secondOneHotSize: Option[Int] = None) {
    @tailrec
    def saveNextFile(toSave: Seq[Address], rest: Seq[Address], fileNumber: Int = 0): Unit = {
      def addressToLine(firstOneHotSize: Int)(address: Address, baseLine: String): String = {
        val firstIdx = address.addressIdx.get / firstOneHotSize
        val secondIdx = firstOneHotSize + address.addressIdx.get % firstOneHotSize

        val firstHotSet = FileUtil.oneHot(firstIdx, baseLine)
        FileUtil.oneHot(secondIdx, firstHotSet)
      }

      if (!toSave.isEmpty) {
        saveOneHotBasedToFile(toSave,
          fileName(fileNumber),
          header,
          fileNumber,
          addressToLine(firstOneHotSize),
          firstOneHotSize: Int,
          secondOneHotSize)
      }

      if (!rest.isEmpty) {
        val (newToSave, newRest) = rest.splitAt(linesPerFile)
        saveNextFile(newToSave, newRest, fileNumber + 1)
      } else {
        println("rest is empty")
      }
    }

    saveNextFile(toSave: Seq[Address], rest: Seq[Address])
  }

  //todo: rename
  private[this] def saveOneHotBasedToFile(
              addresses: Seq[Address],
              fileName: String,
              header: String,
              fileNumber: Int,
              addressToLine: (Address, String) => String,
              firstOneHotSize: Int,
              secondOneHotSize: Option[Int] = None): Unit = {
    val file = new PrintWriter(new File(fileName))
    try {
      file.write(header + "\n")
      val base1Hot = FileUtil.baseEmptyLine(firstOneHotSize + secondOneHotSize.getOrElse(0))

      addresses.foreach(address => {
        val firstColumnIdx = context.firstColumnValue(address)
        val dataLine = addressToLine(address, base1Hot)

        file.write(firstColumnIdx + internalDelimiter + dataLine + "\n")
      })
    } finally {
      file.close()
    }
  }
}
