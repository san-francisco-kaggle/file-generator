package sf_crime.address

import sf_crime.utils.ContextDependentEntity

case class Address(address: String,
                   categoryIdx: Option[Int],
                   id:Option[Int],
                   addressIdx: Option[Int]= None) extends ContextDependentEntity
