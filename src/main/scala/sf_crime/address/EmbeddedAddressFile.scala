package sf_crime.address

import sf_crime.utils.{FilePaths, RunContext}

object EmbeddedAddressFile {

  def main(args: Array[String]): Unit = {
    create()
  }

  def create(): Unit = {
    EmbeddedAddressTrain.create()
    EmbeddedAddressTest.create()
  }
}

object EmbeddedAddressTrain extends AddressFunctions {
  def context = RunContext.trainContext

  def create(): Unit = {
    createAddressesToIndexes(0)
    createMultipleHotAddressFile(FilePaths.trainDataAddressMultiple)
  }
}

object EmbeddedAddressTest extends AddressFunctions {
  def context = RunContext.testContext

  def create(): Unit ={
    createMultipleHotAddressFile(FilePaths.testDataAddressMultiple)
  }
}

