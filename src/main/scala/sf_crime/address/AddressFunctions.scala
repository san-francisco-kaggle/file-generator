package sf_crime.address

import sf_crime._
import sf_crime.utils.{FilePaths, FileUtil, ToIndexFile}

import scala.io.BufferedSource

trait AddressFunctions extends OneHotFunctions with ToIndexFile {

  @deprecated("Use createMultipleHotAddressFile")
  def createOneHotAddressFile(toFilePre: String) = {
    createAddressFile(toFilePre, saveAsOneHotFiles, countLimit)
  }

  def createMultipleHotAddressFile(toFile: String) = {
    createAddressFile(toFile, saveAsMultipleHotFile)
  }

  def createAddressesToIndexes(truncateIfLess: Int = countLimit) = {
    val dataReader = io.Source.fromFile(context.dataPath)
    try {
      val addresses = getAddresses(dataReader)
      createIndexesToFile(addresses.map(_.address), addressToIndexFileName(truncateIfLess), truncateIfLess)
    } finally {
      dataReader.close()
    }
  }

  private[this] def addressToIndexFileName(limit: Int = countLimit) = FilePaths.addressToIndexPre + limit + csv

  private[this] def getAddresses(dataReader: BufferedSource) = {
    def lineToAddress(categoryToIndexMap: Map[String, Int])(line: String): Option[Address] = {
      if (context.isHeaderLine(line))
        None
      else {
        val cols = line.split(externalDelimiter)
        val categoryIdx = context.getCategoryIdx(categoryToIndexMap, cols)
        val address = cols(context.columnIdx.addressIdx)
        val id = context.getId(cols)

        Some(Address(address, categoryIdx, id))
      }
    }


    val categoryToIndexMap = FileUtil.getCategoryIndexMap
    dataReader.getLines().map(lineToAddress(categoryToIndexMap)).flatten.toSeq
  }

  private[this] def createAddressFile(toFilePre: String,
                                      saveToFile: (Seq[Address],String) => Unit,
                                      limit: Int = 0) = {
    val dataReader = io.Source.fromFile(context.dataPath)
    try {
      val addresses = getAddresses(dataReader)
      val addressAsIndexes = convertAddressToIndexes(addresses, limit)

      saveToFile(addressAsIndexes, toFilePre)
    } finally {
      dataReader.close()
    }
  }

  private[this] def convertAddressToIndexes(addresses: Seq[Address], limit: Int) = {
    def getAddressToIndexMap(): Map[String, Int] = {
      FileUtil.getNameToIndexMap(addressToIndexFileName(limit))
    }
    val addressToIndex = getAddressToIndexMap()

    addresses.map(address => {
      val addressIdx = addressToIndex.getOrElse(address.address, notDefined._2)
      address.copy(addressIdx = Some(addressIdx))
    })
  }

}

