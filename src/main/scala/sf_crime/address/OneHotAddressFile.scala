package sf_crime.address

import sf_crime.utils._

object OneHotAddressFile {

  def main(args: Array[String]): Unit = {
    create()
  }

  @deprecated
  def create(): Unit = {
    OneHotAddressTrain.create()
    OneHotAddressTest.create()
  }
}

object OneHotAddressTrain extends AddressFunctions {
  def context = RunContext.trainContext

  @deprecated
  def create(): Unit = {
    createAddressesToIndexes()
    createOneHotAddressFile(FilePaths.trainDataAddressPre)
  }
}

object OneHotAddressTest extends AddressFunctions {
  def context = RunContext.trainContext

  @deprecated
  def create(): Unit = {
    createOneHotAddressFile(FilePaths.testDataAddressPre)
  }
}