package sf_crime.address

trait AddressConstants {
  // How many times address must be found before it will be taken to training file.
  val countLimit = 100

  // If original file has more than linesPerFile lines then results will be written in multiple files.
  val linesPerFile = 1000000
}
