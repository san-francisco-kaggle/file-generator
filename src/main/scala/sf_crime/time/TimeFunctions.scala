package sf_crime.time

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import sf_crime._
import sf_crime.utils.{ContextDependentEntity, FileUtil, HasContext}

trait TimeFunctions extends HasContext {
  private[this] val it = internalDelimiter

  def createTimeFile(toFile: String) = {
    val categoryToIndex = FileUtil.getCategoryIndexMap
      FileUtil.parseAndSave(context, toFile, parseTime(categoryToIndex))
  }

  private[this] def parseTime(categoryToIndex: Map[String, Int])(line: String): Option[String] = {
    def getHeaderLine = {
      context.firstColumnHeader + it +
        s"h0${it}h1${it}h2${it}h3${it}h4${it}h5${it}h6${it}h7${it}h8${it}h9${it}h10${it}h11${it}h12${it}h13${it}h14${it}" +
        s"h15${it}h16${it}h17${it}h18${it}h19${it}h20${it}h21${it}h22${it}h23${it}" +
        s"w0${it}w1${it}w2${it}w3${it}w4${it}w5${it}w6${it}" +
        s"m0${it}m1${it}m2${it}m3${it}m4${it}m5${it}m6${it}m7${it}m8${it}m9${it}m10${it}m11"
    }

    if(context.isHeaderLine(line)) {
      Some(getHeaderLine)
    } else {
      Some(getDataLine(line, categoryToIndex))
    }
  }

  private[this] def getDataLine(line: String, categoryToIndex: Map[String, Int]) = {
    val time = parseTime(line, categoryToIndex)

    val hourHot = FileUtil.parseOneHot(23, 1, time.hourIdx)
    val monthHot = FileUtil.parseOneHot(11, 1, time.monthIdx)
    val weekdayHot = FileUtil.parseOneHot(6, 1, time.weekDayIdx)
    context.firstColumnValue(time) + it + hourHot + it + weekdayHot + it + monthHot
  }

  private[this] def parseTime(line: String, categoryToIndex: Map[String, Int]): Time = {
    def parseDateTime(dateTime: String): DateTime = {
      val dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss Z")
      dtf.parseDateTime(dateTime + " -08:00")
    }

    val indexes = context.columnIdx
    val cols = line.split(externalDelimiter).map(_.trim)
    val timeAndMonth = cols(indexes.timeIdx)
    val dateTime = parseDateTime(timeAndMonth)

    val hourIdx = dateTime.hourOfDay().get()
    val monthIdx = dateTime.monthOfYear().get() - 1
    val weekday = cols(indexes.weekdayIdx)
    val weekDayIdx = getWeekDayIndex(weekday)
    val categoryIdx = context.getCategoryIdx(categoryToIndex, cols)
    val id = context.getId(cols)
    Time(hourIdx, monthIdx, weekDayIdx, categoryIdx, id)
  }

  private[this] def getWeekDayIndex(weekday: String): Int = {
    weekday match {
      case "Monday" => 0
      case "Tuesday" => 1
      case "Wednesday" => 2
      case "Thursday" => 3
      case "Friday" => 4
      case "Saturday" => 5
      case "Sunday" => 6
      case weekDay: String => throw new RuntimeException("Illegal weekday: " + weekDay)
    }
  }
}

