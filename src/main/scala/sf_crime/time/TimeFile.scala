package sf_crime.time

import sf_crime.utils._

object TimeFile {
  def main(args: Array[String]): Unit = {
    create()
  }

  def create() = {
    TimeTrain.create()
    TimeTest.create()
  }
}

object TimeTrain extends TimeFunctions {
  def context = RunContext.trainContext

  def create(): Unit = {
    createTimeFile(FilePaths.trainDataTime)
  }
}

object TimeTest extends TimeFunctions {
  def context = RunContext.testContext

  def create(): Unit = {
    createTimeFile(FilePaths.testDataTime)
  }
}
