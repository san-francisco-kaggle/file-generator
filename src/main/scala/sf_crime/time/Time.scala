package sf_crime.time

import sf_crime.utils.ContextDependentEntity

case class Time(hourIdx: Int,
                monthIdx: Int,
                weekDayIdx: Int,
                categoryIdx: Option[Int],
                id: Option[Int]) extends ContextDependentEntity
