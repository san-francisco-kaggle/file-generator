package sf_crime.coordinates

import sf_crime.utils.{FilePaths, RunContext}

object CoordFile {

  def main(args: Array[String]): Unit = {
    create()
  }

  def create() = {
    CoordTrain.create()
    CoordTest.create()
  }
}

object CoordTrain extends CoordFunctions {
  def context = RunContext.trainContext

  def create(): Unit = {
    createCoordFileTrain(FilePaths.trainDataCoord)
  }
}

object CoordTest extends CoordFunctions {
  def context = RunContext.testContext

  def create(): Unit = {
    createCoordFileTest(FilePaths.testDataCoord)
  }
}
