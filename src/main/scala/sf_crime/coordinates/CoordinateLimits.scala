package sf_crime.coordinates

import sf_crime.utils.enrichDouble.DoubleEnrich._

case class CoordinateLimits(xMax: Option[Double] = None,
                            xMin: Option[Double] = None,
                            yMax: Option[Double] = None,
                            yMin: Option[Double] = None) {

  private[this] val normalizedMissing = 1.1
  private[this] val badXCoord = -120.5
  private[this] val badYCoord = 90.0

  def update(c: Coordinate): CoordinateLimits = {
    if(isMissingCoordinate(c.X, c.Y)) {
      this
    } else {
      val newMaxX = newMax(xMax, c.X)
      val newMinX = newMin(xMin, c.X)
      val newMaxY = newMax(yMax, c.Y)
      val newMinY = newMin(yMin, c.Y)

      CoordinateLimits(newMaxX, newMinX, newMaxY, newMinY)
    }
  }

  def isLimitsSet: Boolean =
    xMax.isDefined && xMin.isDefined && yMax.isDefined && yMin.isDefined

  def normalizeX(xCoord: Double): Double = {
    if(isMissingXCoordinate(xCoord))
      normalizedMissing
    else
     normalize(xMax.get, xMin.get, xCoord)
  }

  def normalizeY(yCoord: Double): Double = {
    if(isMissingYCoordinate(yCoord))
      normalizedMissing
    else
      normalize(yMax.get, yMin.get, yCoord)
  }

  private[this] def normalize(max: Double, min: Double, value: Double): Double = {
    1 / (max - min) * (value - min)
  }

  private[this] def newMost(old: Option[Double], candidate: Double, selectMost: (Double, Double) => Double) = {
    old match {
      case Some(o) =>
        selectMost(o, candidate)
      case None =>
        candidate
    }
  }

  private[this] def newMax(oldLargest: Option[Double], candidate: Double):Option[Double] = {
    Some(newMost(oldLargest, candidate, math.max))
  }

  private[this] def newMin(oldSmallest: Option[Double], candidate: Double):Option[Double] = {
    Some(newMost(oldSmallest, candidate, math.min))
  }

  private[this] def isMissingCoordinate(xCoord: Double, yCoord: Double): Boolean = {
    isMissingXCoordinate(xCoord) ||
      isMissingYCoordinate(yCoord)
  }

  private [this] def isMissingXCoordinate(xCoord: Double) = {
    xCoord.almostEqual(badXCoord)
  }

  private[this] def isMissingYCoordinate(yCoord: Double) = {
    yCoord.almostEqual(badYCoord)
  }

}

