package sf_crime.coordinates

import java.io.{File, PrintWriter}

import sf_crime.utils.{FileUtil, HasContext}

import scala.io.BufferedSource
import sf_crime._

trait CoordinatesIo  extends HasContext{
  // BufferedSource has to be opened and closed in calling method since opening&closing it in this method would
  // cause exception when returned Seq[Coordinates] is used.
  def readCoordinates(fromFile: BufferedSource): Seq[Coordinate] = {
    val categoryToIndex = FileUtil.getCategoryIndexMap
    fromFile.getLines().flatMap(parseCoordinates(categoryToIndex) _ ).toSeq
  }

  def writeCoordinates(coordinates: Seq[Coordinate], toFile: String) {
    def getCoordinateDataLine(coord: Coordinate) = {
      context.firstColumnValue(coord) + internalDelimiter +
        s"${"%.3f".format(coord.X)}$internalDelimiter${"%.3f".format(coord.Y)}\n"
    }
    def getCoordinateHeaderLine =
      context.firstColumnHeader + internalDelimiter + "X" + internalDelimiter +"Y\n"

    val trainFile = new PrintWriter(new File(toFile))

    try {
      trainFile.write(getCoordinateHeaderLine)
      coordinates.foreach(coord => trainFile.write(getCoordinateDataLine(coord)))
    } finally {
      trainFile.close()
    }
  }

  private[this] def parseCoordinates(categoryToIndex: Map[String, Int])
                                    (line: String): Option[Coordinate] = {
    val indexes = context.columnIdx
    if(!context.isHeaderLine(line)) {
      // No missing or illegally formatted coordinates are expected
      val cols = line.split(externalDelimiter).map(_.trim)

      val xCoord: Double = cols(indexes.xIdx).toDouble
      val yCoord: Double = cols(indexes.yIdx).toDouble
      val categoryIdx = context.getCategoryIdx(categoryToIndex, cols)
      val id = context.getId(cols)

      Some(Coordinate(xCoord, yCoord, categoryIdx, id))
    } else None
  }
}