package sf_crime.coordinates

import sf_crime.utils.ContextDependentEntity

case class Coordinate(X: Double, Y: Double, categoryIdx: Option[Int], id: Option[Int]) extends ContextDependentEntity
