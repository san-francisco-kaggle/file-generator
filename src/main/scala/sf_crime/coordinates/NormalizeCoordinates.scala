package sf_crime.coordinates

import java.io.{File, PrintWriter}

import sf_crime.utils.FilePaths

import scala.annotation.tailrec
import sf_crime._

trait NormalizeCoordinates {

  def convertToNormalizedCoordinatesTest(coordinates: Seq[Coordinate]) = {
    val coordLimits = loadCoordinateLimits()
    printCoordLimits(coordLimits)
    normalizeCoordinates(coordLimits, coordinates)
  }

  def convertToNormalizedCoordinatesTrain(coordinates: Seq[Coordinate]): Seq[Coordinate] = {
    val coordLimits = getCoordinateLimits(coordinates)
    printCoordLimits(coordLimits)
    saveCoordsLimitsForTesting(coordLimits)
    normalizeCoordinates(coordLimits, coordinates)
  }

  private def normalizeCoordinates(coordLimits: CoordinateLimits, coordinates: Seq[Coordinate]) = {
    coordinates.map(coordinate => {
      Coordinate(coordLimits.normalizeX(
        coordinate.X),
        coordLimits.normalizeY(coordinate.Y),
        coordinate.categoryIdx,
        coordinate.id)
    })
  }

  @tailrec
  private def getCoordinateLimits(coordinates: Seq[Coordinate],
                                  coordinateLimits: CoordinateLimits = CoordinateLimits()):CoordinateLimits = {
    coordinates match {
        case Nil => coordinateLimits
        case head +: tail => getCoordinateLimits(tail, coordinateLimits.update(head))
      }
  }

  private[this] def loadCoordinateLimits(): CoordinateLimits = {
    def parseCoordinateLimits(cols: Array[String]) = {
      val xMax = cols(0).toDouble
      val xMin = cols(1).toDouble
      val yMax = cols(2).toDouble
      val yMin = cols(3).toDouble

      CoordinateLimits(Some(xMax), Some(xMin), Some(yMax), Some(yMin))
    }

    val bufferedSource = io.Source.fromFile(FilePaths.coordLimits)
    try {
      val lines = bufferedSource.getLines().toSeq
      if(lines.length != 1)
        throw new RuntimeException("Loading Coordinate Limits failed")
      else {
        val cols = lines.head.split(internalDelimiter).map(_.trim)
        parseCoordinateLimits(cols)
      }
    } finally {
      bufferedSource.close()
    }
  }

  private[this] def printCoordLimits(coordLimits: CoordinateLimits) = {
    println(coordLimits.xMax, "xMax")
    println(coordLimits.xMin, "xMin")
    println(coordLimits.yMax, "yMax")
    println(coordLimits.yMin, "yMin")
    println(coordLimits.xMax.get - coordLimits.xMin.get, "xMax - xMin")
    println(coordLimits.yMax.get - coordLimits.yMin.get, "yMax - yMin")
  }

  private[this] def saveCoordsLimitsForTesting(coordLimits: CoordinateLimits):Unit = {
    def getNormalizedDataLine(coordLimits: CoordinateLimits) = {
      s"${coordLimits.xMax.get}$internalDelimiter" +
        s"${coordLimits.xMin.get}$internalDelimiter" +
        s"${coordLimits.yMax.get}$internalDelimiter" +
        s"${coordLimits.yMin.get}"
    }

    if(!coordLimits.isLimitsSet)
      throw new RuntimeException("Something went wrong. Coordinate limits not found")

    val normalizationWriter = new PrintWriter(new File(FilePaths.coordLimits))
    try {
      normalizationWriter.write(getNormalizedDataLine(coordLimits))
    } finally {
      normalizationWriter.close()
    }
  }
}
