package sf_crime.coordinates

import sf_crime.utils.HasContext

trait CoordFunctions extends CoordinatesIo with NormalizeCoordinates with HasContext {
  def createCoordFileTrain(toFile: String): Unit = {
    createCoordFile(toFile, convertToNormalizedCoordinatesTrain)
  }

  def createCoordFileTest(toFile: String): Unit = {
    createCoordFile(toFile, convertToNormalizedCoordinatesTest)
  }

  private[this] def createCoordFile(toFile: String, convertToNormalizedCoordinates: (Seq[Coordinate]) =>  Seq[Coordinate]) {
    val bufferedSource = io.Source.fromFile(context.dataPath)
    try {
      val coordinates: Seq[Coordinate] = readCoordinates(bufferedSource)
      val normalizedCoordinates = convertToNormalizedCoordinates(coordinates)
      writeCoordinates(normalizedCoordinates, toFile)
    } finally {
      bufferedSource.close()
    }
  }
}
