package sf_crime.pdDistrict

import sf_crime._
import sf_crime.utils._

import scala.io.BufferedSource

trait PdDistrictFunctions extends HasContext with ToIndexFile {

  // todo: there is few places where indexes are created. Perhaps there is some dublicate code
  def createPdDistrictToIndexes() {
    val dataReader = io.Source.fromFile(context.dataPath)
    try {
      val pdDistricts = getPdDistrictsNames(dataReader)
      createIndexesToFile(pdDistricts, FilePaths.pdDistrictToIndex)
    } finally {
      dataReader.close()
    }
  }

  def createPdDistrictFile(toFile: String): Unit = {
    val categoryToIndex = FileUtil.getCategoryIndexMap
    val pdDistrictToIndex = FileUtil.getNameToIndexMap(FilePaths.pdDistrictToIndex)

    val pdDistrictMaxIndex = pdDistrictToIndex.values.max
    val baseEmptyLine = FileUtil.baseEmptyLine(pdDistrictMaxIndex)
    FileUtil.parseAndSave(context, toFile, parsePdDistrict(categoryToIndex, pdDistrictToIndex, baseEmptyLine))
  }

  private[this] def getPdDistrictsNames(dataReader: BufferedSource) = {
    val categoryToIndexMap = FileUtil.getCategoryIndexMap
    dataReader.getLines().map(mapLineToPdDistricts(categoryToIndexMap)).flatten.toSeq
  }

  private[this] def mapLineToPdDistricts(categoryToIndexMap: Map[String, Int])(line: String): Option[String] = {
    if (context.isHeaderLine(line))
      None
    else {
      val cols = line.split(externalDelimiter)
      val pdDistrict = cols(context.columnIdx.pdDistrictIdx)
      Some(pdDistrict)
    }
  }

  private[this] def parsePdDistrict(categoryToIndex: Map[String, Int],
                                    pdDistrictToIndex: Map[String, Int],
                                    baseEmptyLine: String)
                                   (line: String): Option[String] = {
    def getPdDistrict(cols: Array[String]) = {
      val pdDistrictName = cols(context.columnIdx.pdDistrictIdx)
      val pdDistrictIdx = pdDistrictToIndex(pdDistrictName)

      val categoryIdx = context.getCategoryIdx(categoryToIndex, cols)
      val id = context.getId(cols)
      PdDistrict(pdDistrictIdx, categoryIdx, id)
    }

    if(context.isHeaderLine(line)) {
      Some(context.firstColumnHeader + internalDelimiter + "pdDistrictIdxs...")
    } else {
      val cols = line.split(externalDelimiter).map(_.trim)
      val pdDistrict = getPdDistrict(cols)
      val oneHotLine = FileUtil.oneHot(pdDistrict.districtIdx, baseEmptyLine)

      Some(context.firstColumnValue(pdDistrict) + internalDelimiter + oneHotLine)
    }
  }
}
