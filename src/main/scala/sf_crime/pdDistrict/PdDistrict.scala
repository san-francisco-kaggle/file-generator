package sf_crime.pdDistrict

import sf_crime.utils.ContextDependentEntity

case class PdDistrict(districtIdx: Int,
                      categoryIdx: Option[Int],
                      id: Option[Int]) extends ContextDependentEntity
