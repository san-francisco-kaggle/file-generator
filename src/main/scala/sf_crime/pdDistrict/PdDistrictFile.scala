package sf_crime.pdDistrict

import sf_crime.utils._

object PdDistrictFile {

  def main(args: Array[String]): Unit = {
    create()
  }

  def create() = {
    PdDistrictTrain.create()
    PdDistrictTest.create()
  }
}

object PdDistrictTrain extends PdDistrictFunctions {
  def context = RunContext.trainContext

  def create() = {
    createPdDistrictToIndexes()
    createPdDistrictFile(FilePaths.trainDataPdDistrict)
  }
}

object PdDistrictTest extends PdDistrictFunctions {
  def context = RunContext.testContext

  def create() = {
    createPdDistrictFile(FilePaths.testDataPdDistrict)
  }
}
