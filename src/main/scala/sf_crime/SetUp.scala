package sf_crime

import sf_crime.address.EmbeddedAddressFile
import sf_crime.combineFiles.CombinedFile
import sf_crime.coordinates.CoordFile
import sf_crime.maintenanceRuns.InitializeData
import sf_crime.pdDistrict.PdDistrictFile
import sf_crime.time.TimeFile

object SetUp {
  def main(args: Array[String]) {
    InitializeData.create()
    EmbeddedAddressFile.create()
    CoordFile.create()
    PdDistrictFile.create()
    TimeFile.create()
    CombinedFile.create()
  }
}
