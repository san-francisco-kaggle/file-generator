package sf_crime.maintenanceRuns

import sf_crime.utils.{FilePaths, RunContext}

object LinesInFile {

  def main(args: Array[String]): Unit = {
    def countLines(filepath: String): Int = {
      val bufferedSource = io.Source.fromFile(filepath)
      try {
        bufferedSource.getLines().count(!_.isEmpty)
      } finally {
        bufferedSource.close()
      }
    }

    println("original")
    println(countLines(RunContext.trainContext.rawDataPath))
    println("cleaned original")
    println(countLines(RunContext.trainContext.dataPath))

    println("time train")
    println(countLines(FilePaths.trainDataTime))
    println("coords train")
    println(countLines(FilePaths.trainDataCoord)) //884262
    println("prediction size, should be 884,262. and is ")
    println(countLines(FilePaths.predictions))
    println("sample submission")
    println(countLines(FilePaths.sampleSub))
  }
}
