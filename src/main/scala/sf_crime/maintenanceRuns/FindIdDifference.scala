package sf_crime.maintenanceRuns

import sf_crime.utils.{FilePaths, FileVariables}
import sf_crime._

object FindIdDifference extends FileVariables{

  def main(args: Array[String]): Unit = {
    comparePredictionsAndSampleFile()
  }

  private[this] def getId(line: String) = {
    if (line.contains("Id") || line.contains("id")) {
      None
    } else {
      val id = line.split(externalDelimiter)(0).toInt
      Some(id)
    }
  }

  private[this] def containsId(idToFind: Int, delimitter: String)(line: String): Boolean = {
    if (line.contains("Id") || line.contains("id")) {
      false
    } else {
      val id = line.split(delimitter)(0).toInt
      if (id == idToFind) {
        true
      } else false
    }
  }

  private[this] def comparePredictionsAndSampleFile() = {
    // Kaggle provided sample file to show how return file should look like. That is sample file
    val sampleFile =  io.Source.fromFile(FilePaths.sampleSub)
    val predictionRawFile = io.Source.fromFile(FilePaths.predictionsRaw)
    val predictionFile = io.Source.fromFile(FilePaths.predictions)
    try {
      val sampleSeq = sampleFile.getLines().toSeq
      val predictionRawSeq = predictionRawFile.getLines().toSeq
      val predictionSeq = predictionFile.getLines().toSeq

      val lastExpectedSample = sampleSeq.find(containsId(numberOfDataLines -1,externalDelimiter))
      val lastExpectedPredRaw = predictionSeq.find(containsId(numberOfDataLines -1,externalDelimiter))
      val lastExpectedPred = predictionRawSeq.find(containsId(numberOfDataLines -1,internalDelimiter))

      println(lastExpectedSample)
      println(lastExpectedPredRaw)
      println(lastExpectedPred)

      val sampleIds = sampleSeq.flatMap(getId)
      val predictionIds = predictionSeq.flatMap(getId)

      val diff = sampleIds.diff(predictionIds)
      diff.foreach(println)
    } finally {
      predictionFile.close()
      predictionRawFile.close()
      sampleFile.close()
    }

  }

}
