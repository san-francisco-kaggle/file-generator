package sf_crime.maintenanceRuns

import sf_crime.utils.{CreateCategoryMapIndexFile, FileUtil, RunContext}

object InitializeData extends CreateCategoryMapIndexFile  {

  def create(): Unit = {
    FileUtil.cleanData(RunContext.trainContext)
    FileUtil.cleanData(RunContext.testContext)
    createIndexMapCategory(RunContext.trainContext)
  }

}
