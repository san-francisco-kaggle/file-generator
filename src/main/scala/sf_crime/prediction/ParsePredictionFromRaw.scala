package sf_crime.prediction

import java.io.{File, PrintWriter}

import sf_crime.utils.FilePaths


// This object is deprecated. Some python codes still uses this so this is not removed.
object ParsePredictionFromRaw extends ParsePredictionFunctions  with ParsePredictionFromRawFunctions {
  @deprecated
  def main(args: Array[String]): Unit = {
    parse()
  }

  // todo: almost complete dublicate with ParsePredictionFromRawVec.parse()
  @deprecated
  def parse() {
    parse(FilePaths.predictionsRaw, getPredictionsToSave)
  }
}
