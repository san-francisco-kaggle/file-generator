package sf_crime.prediction

import sf_crime._

@deprecated
trait ParsePredictionFromRawFunctions {
  self: ParsePredictionFunctions =>
  @deprecated
  val mostCommonCategoryIdx = 16

  @deprecated
  def getPredictionsToSave(predictionLines:  Iterator[String]) = {
    def buildMissingObject(x: Int): Prediction = {
      Prediction(x, mostCommonCategoryIdx)
    }

    val sortedPredictions = getPredictions(predictionLines).sortWith(sortAsc)
    // Finding missing predictions, then adding then sorting again. This was pretty fast so this was selected
    val missingPredictions = findMissingPredictions(sortedPredictions, buildMissingObject)
    (missingPredictions ++ sortedPredictions).sortWith(sortAsc)
  }

  @deprecated
  private[this] def getPredictions(lines: Iterator[String]) = {
    lines.map(line => {
      if(!line.contains("id")) {
        val id = line.split(internalDelimiter)(0).toInt
        val categoryIdx = line.split(internalDelimiter)(1).toInt

        Some(Prediction(id, categoryIdx))
      } else None
    }).toSeq.flatten
  }
}
