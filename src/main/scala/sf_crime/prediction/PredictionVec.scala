package sf_crime.prediction

case class PredictionVec(id: Int, classes: String) extends HasId {
  override def toString(): String = {
    id + classes
  }
}
