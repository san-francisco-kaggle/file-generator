package sf_crime.prediction

import sf_crime._

trait ParsePredictionFromRawVecFunctions {
  self: ParsePredictionFunctions =>

  private[this] val e = externalDelimiter

  val genericMissingCategory = s"${e}0.00455857${e}0.0909926${e}0.00455857${e}0.00455857${e}0.03889116${e}0.00455857${e}0.00455857${e}0.06038359${e}0.00455857${e}0.00455857${e}0.00455857${e}0.00455857${e}0.00455857${e}0.01968569${e}0.00455857${e}0.00455857${e}0.17150544${e}0.00455857${e}0.00455857${e}0.03009543${e}0.09492133${e}0.14124191${e}0.00455857${e}0.00910212${e}0.00455857${e}0.02647365${e}0.00455857${e}0.01100806${e}0.00455857${e}0.00455857${e}0.00455857${e}0.00455857${e}0.03621175${e}0.00455857${e}0.00455857${e}0.0458926${e}0.06385308${e}0.05033579${e}0.00455857"

  def getPredictionsVecToSave(predictionLines:  Iterator[String]) = {
    def buildMissingObject(id: Int): PredictionVec = {
      PredictionVec(id, genericMissingCategory)
    }

    val sortedPredictionVecs = getPredictionsVec(predictionLines).sortWith(sortAsc)
    val missingPredictions = findMissingPredictions(sortedPredictionVecs, buildMissingObject)
    (missingPredictions ++ sortedPredictionVecs).sortWith(sortAsc)
  }

  private[this] def getPredictionsVec(rawVecs: Iterator[String]): Seq[PredictionVec] = {
    rawVecs.map(line => {
      val idEndIndex = line.indexOf(externalDelimiter)
      val (id, classes) = line.splitAt(idEndIndex)
      PredictionVec(id.toInt, classes)
    }).toSeq
  }
}
