package sf_crime.prediction

import sf_crime._

@deprecated
case class Prediction(id: Int, categoryIdx: Int) extends HasId {
  private[this] val e = externalDelimiter
  val predictionBaseData = s"0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0${e}0"

  override def toString(): String = {
    "\n" + id + e + oneHot(categoryIdx)
  }

  private[this] def oneHot(categoryIndex: Int): String = {
    predictionBaseData.updated(2 * categoryIndex,"1").mkString
  }

}

