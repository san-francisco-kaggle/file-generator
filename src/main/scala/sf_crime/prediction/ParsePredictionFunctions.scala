package sf_crime.prediction

import java.io.{File, PrintWriter}

import sf_crime._
import sf_crime.utils.FilePaths

import scala.annotation.tailrec

trait HasId {
  val id: Int
}

trait ParsePredictionFunctions {

  private[this] val e = externalDelimiter
  val predictionHeader = s"Id${e}ARSON${e}ASSAULT${e}BAD CHECKS${e}BRIBERY${e}BURGLARY${e}DISORDERLY CONDUCT${e}DRIVING UNDER THE INFLUENCE${e}DRUG/NARCOTIC${e}DRUNKENNESS${e}EMBEZZLEMENT${e}EXTORTION${e}FAMILY OFFENSES${e}FORGERY/COUNTERFEITING${e}FRAUD${e}GAMBLING${e}KIDNAPPING${e}LARCENY/THEFT${e}LIQUOR LAWS${e}LOITERING${e}MISSING PERSON${e}NON-CRIMINAL${e}OTHER OFFENSES${e}PORNOGRAPHY/OBSCENE MAT${e}PROSTITUTION${e}RECOVERED VEHICLE${e}ROBBERY${e}RUNAWAY${e}SECONDARY CODES${e}SEX OFFENSES FORCIBLE${e}SEX OFFENSES NON FORCIBLE${e}STOLEN PROPERTY${e}SUICIDE${e}SUSPICIOUS OCC${e}TREA${e}TRESPASS${e}VANDALISM${e}VEHICLE THEFT${e}WARRANTS${e}WEAPON LAWS"

  def parse(fromFile: String, getPrediction: (Iterator[String]) => Seq[Any]) = {
    val predictionRawFile = io.Source.fromFile(fromFile)
    val predictionsFile = new PrintWriter(new File(FilePaths.predictions))

    try {
      // def getPredictionsVecToSave(predictionLines: scala.Iterator[String]): Seq[PredictionVec]
      val completePredictions = getPrediction(predictionRawFile.getLines())
      predictionsFile.write(predictionHeader + "\n")
      completePredictions.foreach(pred => {
        predictionsFile.write(pred.toString() + "\n")
      })

    } finally {
      predictionRawFile.close()
      predictionsFile.close()
    }
  }

  def sortAsc(pred1: HasId, pred2:HasId): Boolean = {
    pred1.id < pred2.id
  }

  // Does not work if last prediction is missing
  @tailrec
  final def findMissingPredictions[T <: HasId](predictionsSortedWithId: Seq[T],
                                                       buildMissingObject: Int => T,
                                                       missing: Seq[T] = Seq(),
                                                       prev: Option[T] = None):Seq[T] = {

    def isPredictionMissing(currentId: Int, previousId: Int): Boolean = {
      if(currentId -1 == previousId) false
      else if(currentId -1 > previousId) true
      else throw new RuntimeException("Bad sorting")
    }

    def getMissingPredictions(current:HasId, previous: HasId): Seq[T] = {
      val missingIdLower = previous.id + 1
      val missingIdHigher = current.id - 1
      for (x <- missingIdLower to missingIdHigher) yield {
        buildMissingObject(x)
      }
    }

    (predictionsSortedWithId, prev) match {
      case (Nil, _) => missing
      case (head +: tail, None) => findMissingPredictions(tail, buildMissingObject, missing, Some(head))
      case (head +: tail, Some(previous)) =>
        if(isPredictionMissing(head.id, previous.id)){
          val missingPredictions = getMissingPredictions(head, previous)
          findMissingPredictions(tail, buildMissingObject, missing ++ missingPredictions, Some(head))
        } else findMissingPredictions(tail, buildMissingObject, missing, Some(head))
    }
  }
}
