package sf_crime.prediction

import java.io.{File, PrintWriter}

import sf_crime._
import sf_crime.utils.FilePaths

import scala.io.BufferedSource

object ParsePredictionFromRawVec extends ParsePredictionFunctions with ParsePredictionFromRawVecFunctions {

  def main(args: Array[String]): Unit = {
    parse()
  }

  def parse(): Unit = {
    parse(FilePaths.predictionsVec, getPredictionsVecToSave)
  }
}
