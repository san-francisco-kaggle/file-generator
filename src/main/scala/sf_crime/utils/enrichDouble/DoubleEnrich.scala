package sf_crime.utils.enrichDouble

import scala.math._

object DoubleEnrich {
  private[this] val doubleDelta = 0.00001

  implicit class DoubleEnrich(val1: Double) {
    def almostEqual(val2: Double): Boolean = {
      abs(val1 - val2) < doubleDelta
    }
  }
}