package sf_crime.utils
import sf_crime._

import java.io.{File, PrintWriter}

trait CreateCategoryMapIndexFile extends ToIndexFile {
  def createIndexMapCategory(context: RunContext): Unit = {
    def getCategoryName(line:String)= {
      if (!context.isHeaderLine(line)) {
        val cols = line.split(externalDelimiter)
        Some(cols(context.columnIdx.categoryIdx.get))
      } else None
    }

    val bufferedSource = io.Source.fromFile(context.dataPath)
    try {
      val categoryNames = bufferedSource.getLines().flatMap(getCategoryName).toSeq.distinct
      createIndexesToFile(categoryNames, FilePaths.categoryToIndex, includeUndefined = false)
    } finally {
      bufferedSource.close()
    }
  }
}
