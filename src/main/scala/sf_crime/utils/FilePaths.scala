package sf_crime.utils

import sf_crime._

// todo: not the same paths are in python and here. Should be only in one place
object FilePaths {
  val dataFile = "generatedData/"

  val categoryToIndex = dataFile + "categoryToIndex" + csv
  val addressToIndexPre = dataFile + "addressToIndex"
  val pdDistrictToIndex = dataFile + "pdDistrictToIndex" + csv

  val coordLimits = dataFile + "coordLimits" + csv

  val trainDataCoord = dataFile + "trainCoord" + csv
  val trainDataTime = dataFile + "trainTime" + csv
  val trainDataPdDistrict = dataFile + "trainPdDistrict" + csv
  val trainDataAddressPre = dataFile + "address/trainAddress"
  val trainDataAddressMultiple = dataFile + "address/trainMultipleOneHot" + csv

  val coordReformatTestData = dataFile + "predictionData/coordReformatTestData" + csv
  val testDataCoord = dataFile + "testCoord" + csv
  val testDataTime = dataFile + "testTime" + csv
  val testDataPdDistrict = dataFile + "testPdDistrict" + csv
  val testDataAddressPre = dataFile + "address/test"
  val testDataAddressMultiple = dataFile + "address/testMultipleOneHot" + csv

  val predictionsRaw = dataFile + "predictionRaw" + csv
  val predictionsVec = dataFile + "predictionsVec" + csv
  val predictions = dataFile + "predictions" + csv
  val sampleSub = dataFile + "sampleSubmission" + csv

  val trainCombinedPre = dataFile + "trainCombined"
  val testCombinedPre = dataFile + "testCombined"
}

trait FileVariables {
  // todo: this must be bad practice
  val numberOfDataLines = 884262
}

