package sf_crime.utils

import sf_crime.utils.RunContext.ColumnIndexes
import sf_crime._

trait ContextDependentEntity {
  def categoryIdx: Option[Int]
  def id: Option[Int]
}

trait HasContext {
  def context: RunContext
}

case class RunContext(
             columnIdx: ColumnIndexes,
             isHeaderLine: (String) => Boolean,
             dataPath: String,
             rawDataPath: String,
             phase: String) {

  def firstColumnHeader =
    if(columnIdx.categoryIdx.isDefined) "categoryIdx"
    else "id"

  def firstColumnValue(contextDependent: ContextDependentEntity) =
    if(columnIdx.categoryIdx.isDefined) contextDependent.categoryIdx.get
    else contextDependent.id.get

  def getCategoryIdx(categoryToIdx: Map[String, Int], cols: Array[String]) = {
    if(columnIdx.categoryIdx.isDefined) Some(categoryToIdx(cols(columnIdx.categoryIdx.get)))
    else None
  }

  def getId(cols: Array[String]) = {
    if(columnIdx.id.isDefined) Some(cols(columnIdx.id.get).toInt)
    else None
  }
}

object RunContext {

  case class ColumnIndexes(id: Option[Int],
                           timeIdx: Int,
                           categoryIdx: Option[Int],
                           weekdayIdx: Int,
                           pdDistrictIdx: Int,
                           addressIdx: Int,
                           xIdx: Int,
                           yIdx: Int)

  private val testPhase = "test"
  private val trainPhase = "train"
  private[this] val testData = "generatedData/testClean.csv"
  private[this] val testDataRaw = "data/test.csv"
  private[this] val trainData = "generatedData/trainClean.csv"
  private[this] val trainDataRaw = "data/train.csv"

  private[this] def isTrainFileHeader(line: String) =
    line.startsWith("Dates" + externalDelimiter + "Categ") || line.startsWith("categoryIdx" + internalDelimiter)

  private[this] def isTestFileHeader(line: String) =
    line.startsWith("Id" + externalDelimiter + "Dates" + externalDelimiter + "D") || line.startsWith("id" + internalDelimiter)

  lazy val trainContext =
    RunContext(ColumnIndexes(None, 0, Some(1), 3, 4, 6, 7, 8), isTrainFileHeader, trainData, trainDataRaw, trainPhase)

  lazy val testContext =
    RunContext(ColumnIndexes(Some(0), 1, None, 2, 3, 4, 5, 6), isTestFileHeader, testData, testDataRaw, testPhase)

}

