package sf_crime.utils

import java.io.{File, PrintWriter}

import sf_crime._

trait ToIndexFile extends FileVariables {
  val notDefined = "NOT_DEFINED" -> 0

  def createIndexesToFile(names: Seq[String], toFile: String, countLimit: Int = 0, includeUndefined: Boolean = true): Unit = {
    val namesToIndex = buildNameToIndex(names, countLimit, includeUndefined)
    saveIndexesToFile(namesToIndex, toFile)
  }

  private[this] def buildNameToIndex(names: Seq[String], nameCountLimit: Int, includeUndefined: Boolean) = {
    def convertToNameWithCount(names: Seq[String]) = {
      names.groupBy(identity)
        .map(groupedByName => (groupedByName._1, groupedByName._2.size))
    }

    val namesMapCount = convertToNameWithCount(names)
    val frequentNames = namesMapCount.filter(_._2 > nameCountLimit)
    printListInfo(frequentNames, namesMapCount, nameCountLimit)

    val namesWithIndex = frequentNames.keys.toSeq.sorted
    if(includeUndefined)
      (notDefined._1 +: namesWithIndex).zipWithIndex
    else namesWithIndex.zipWithIndex
  }

  private[this] def printListInfo(frequentNames: Map[String, Int],
                                  namesWithCount: Map[String, Int],
                                  nameCountLimit: Int): Unit = {
    println(namesWithCount.keys.size,  " unique indexes")
    if(nameCountLimit > 0) {
      println(frequentNames.size, s"indexes with more than $nameCountLimit crimes")
      val sumOfAllCrimesInNameGroup = frequentNames.foldLeft(0)((sum,nameAndCount) => sum + nameAndCount._2)
      println(sumOfAllCrimesInNameGroup.toDouble / numberOfDataLines.toDouble, "share of all crimes")
    }
  }

  private[this] def saveIndexesToFile(toSave: Seq[(String, Int)], toFile: String): Unit = {
    val file = new PrintWriter(new File(toFile))
    try {
      toSave.foreach(lineToSave  =>
        file.write(lineToSave._1 + internalDelimiter + lineToSave._2 + "\n"))
    } finally {
      file.close()
    }
  }
}
