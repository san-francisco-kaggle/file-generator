package sf_crime.utils

import java.io.{File, PrintWriter}

import scala.annotation.tailrec
import scala.io.BufferedSource
import sf_crime._

object FileUtil {

  def baseEmptyLine(maxIndex:Int): String = {
    val baseEmptyLine: IndexedSeq[Int] = IndexedSeq.fill(maxIndex+1){0}
    baseEmptyLine.mkString(internalDelimiter)
  }

  def oneHot(index: Int, baseEmptyLine: String): String = {
    baseEmptyLine.updated(2 * index,"1").mkString
  }

  def parseAndSave(context: RunContext,
                   toFile: String,
                   parseLine: (String) => Option[String]): Unit = {
    val bufferedSource = io.Source.fromFile(context.dataPath)
    val targetFile = new PrintWriter(new File(toFile))
    try {
      val linesToSave = bufferedSource.getLines().map(parseLine).flatten.toSeq
      linesToSave.foreach(lineToSave => {
        targetFile.write(lineToSave + "\n")
      })
    } finally {
      bufferedSource.close()
      targetFile.close()
    }
  }

  def getNameToIndexMap(fromFile: String): Map[String, Int] = {
    val nameToIndexesBuffer = io.Source.fromFile(fromFile)
    try {
      nameToIndexesBuffer.getLines().map(line => {
        val cols = line.split(internalDelimiter)
        cols(0) -> cols(1).toInt
      }).toMap
    } finally {
      nameToIndexesBuffer.close()
    }
  }

  def getCategoryIndexMap = {
    getNameToIndexMap(FilePaths.categoryToIndex)
  }

  def cleanData(context: RunContext) = {
    val bufferedSource = io.Source.fromFile(context.rawDataPath)
    val writeTrainFile = new PrintWriter(new File(context.dataPath))

    try {
      for(lineRaw <- bufferedSource.getLines()) {
        val line = removeIllegalChars(lineRaw)
        if (!line.isEmpty) {
          writeTrainFile.write(line + "\n")
        }
      }

    } finally {
      bufferedSource.close()
      writeTrainFile.close()
    }
  }

  @deprecated // use baseEmptyLine and oneHot instead. It is much faster
  def parseOneHot(maxIndex: Int, divider: Int, hotIndex: Int): String = {
    @tailrec
    def parseHot(currentIndex: Int = 0, lineSoFar: String = "", delimiter: String = ""): String = {
      if(currentIndex > maxIndex)
        lineSoFar
      else if(currentIndex == hotIndex) {
        val newLineSoFar = s"$lineSoFar$delimiter${"%.3f".format(1.0 / divider)}"
        parseHot(currentIndex + 1, newLineSoFar, internalDelimiter)
      }
      else {
        parseHot(currentIndex + 1, s"$lineSoFar$delimiter${0}", internalDelimiter)
      }
    }

    parseHot()
  }

  @tailrec
  private[this] final def removeIllegalChars(line: String): String = {
    def cleanText(line:String, quoteIdxStart: Int, quoteIdxEnd: Int) = {
      val wordToClean = line.substring(quoteIdxStart, quoteIdxEnd + 1)
      val cleanedWord = wordToClean.filterNot(a => a == '\"' || externalDelimiter.contains(a))
      line.substring(0, quoteIdxStart) + cleanedWord + line.substring(quoteIdxEnd + 1)
    }

    val quoteIdxStart = line.indexOf("\"")
    if(quoteIdxStart != -1){
      val quoteIdxEnd = line.indexOf("\"", quoteIdxStart + 1)
      val cleanedLine = cleanText(line, quoteIdxStart, quoteIdxEnd)
        if(cleanedLine.indexOf("\"") != -1) {
        removeIllegalChars(cleanedLine)
      } else cleanedLine
    } else line
  }
}
