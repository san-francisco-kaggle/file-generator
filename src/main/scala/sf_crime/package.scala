/**
  * Created by timonikkila on 26/05/16.
  */
package object sf_crime {
  val externalDelimiter = ","
  val internalDelimiter = ";"

  val csv = ".csv"
}
