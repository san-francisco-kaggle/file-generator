package sf_crime.combineFiles

object WriteStatusTypes {
  trait WriteStatus
  class FileLimit() extends WriteStatus
  class InputEnd() extends WriteStatus
  object FileLimit {
    def apply(): FileLimit = new FileLimit()
  }

  object InputEnd {
    def apply(): InputEnd = new InputEnd()
  }
}