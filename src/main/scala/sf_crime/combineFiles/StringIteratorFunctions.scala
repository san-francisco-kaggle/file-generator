package sf_crime.combineFiles

import sf_crime.utils.HasContext

import scala.annotation.tailrec

trait StringIteratorFunctions extends HasContext {
  @tailrec
  final def getNextNonEmpty(lines: Iterator[String]): Option[String] = {
    if(!lines.hasNext)
      None
    else {
      val newLine = lines.next()
      if (newLine.isEmpty) getNextNonEmpty(lines)
      else Some(newLine)
    }
  }
}
