package sf_crime.combineFiles

import java.io.{File, PrintWriter}

import sf_crime._

import scala.annotation.tailrec
import WriteStatusTypes._

trait CombineFunctions extends StringIteratorFunctions {

  def createCombinedFile(filesToCombine: Seq[String], toFilePre: String, linesPerFile: Int): Unit = {

    val sourceFiles = filesToCombine.map(io.Source.fromFile(_))
    try {
      val sourceLines = sourceFiles.map(_.getLines())
      val headerLines = sourceLines.map(_.next())

      val parsedHeader = combineCsvLines(headerLines)

      saveBatchFiles(sourceLines, toFilePre, linesPerFile, parsedHeader)
    } finally {
      sourceFiles.foreach(_.close())
    }
  }

  @tailrec
  private[this] def saveBatchFiles(sourceLines: Seq[Iterator[String]],
                           toFilePre: String,
                           linesPerFile: Int,
                           header: String,
                           fileNumber: Int = 0): Unit = {
    def saveFile(): WriteStatus = {
      val targetFile = new PrintWriter(new File(s"$toFilePre$fileNumber$csv"))
      try {
        targetFile.write(header +"\n")
        saveBatchFile(sourceLines, targetFile, linesPerFile)
      } finally {
        targetFile.close()
      }
    }

    if(saveFile() == FileLimit())
      saveBatchFiles(sourceLines, toFilePre, linesPerFile, header, fileNumber + 1)
  }


  private[this] def combineCsvLines(dataLines: Seq[String]) = {
    // First column is taken once, since it is the same with all lines.
    dataLines.reduce((combined,next) => {
      val firstColumnIdx = next.indexOf(internalDelimiter)
      combined + next.substring(firstColumnIdx)
    })
  }

  @tailrec
  private[this] def saveBatchFile(sourceLines: Seq[Iterator[String]],
                            targetFile: PrintWriter,
                            linesToGo: Int): WriteStatus = {
    if(linesToGo == 0) FileLimit()

    val nextDataLines = sourceLines.map(getNextNonEmpty)
    if(nextDataLines.forall(_.isDefined)) {
      val dataLine = combineCsvLines(nextDataLines.flatten)

      targetFile.write(dataLine + "\n")
      saveBatchFile(sourceLines, targetFile, linesToGo - 1)
    } else InputEnd()
  }
}
