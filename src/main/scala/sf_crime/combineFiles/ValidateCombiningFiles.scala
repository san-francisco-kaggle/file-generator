package sf_crime.combineFiles

import sf_crime._

import scala.annotation.tailrec

trait ValidateCombiningFiles extends StringIteratorFunctions {

  def validateFileCombining(files: Seq[String]) = {
    def assertHeaderLines(headerLines: Seq[String]) = {
      assert(headerLines.forall(context.isHeaderLine))

      val firstColumns = headerLines.map(_.split(internalDelimiter)(0))
      assert(firstColumns.distinct.length == 1)
    }

    val sourceFiles = files.map(io.Source.fromFile(_))
    try {
      val sourceLines = sourceFiles.map(_.getLines())
      val headerLines = sourceLines.map(_.next())
      assertHeaderLines(headerLines)
      assertDataLines(sourceLines)
    } finally {
      sourceFiles.foreach(_.close())
    }
  }

  @tailrec
  private[this] def assertDataLines(sourceLines: Seq[Iterator[String]], lineNr: Int = 2): Unit = {
    if(sourceLines.forall(_.hasNext)){
      val nextLines =  sourceLines.map(getNextNonEmpty)

      if(nextLines.forall(_.isDefined)){
        val firstColumns = nextLines.map(_.get.split(internalDelimiter)(0))

        if(firstColumns.distinct.length == 1)
          assertDataLines(sourceLines, lineNr + 1)
        else throw new RuntimeException(s"indexes values not matching. Line $lineNr. Indexes: " + firstColumns.distinct)
      } else if(!nextLines.forall(_.isEmpty))
        throw new RuntimeException(s"Some data files ended, not all. Line $lineNr. Ended with empty")

    } else if(sourceLines.forall(!_.hasNext) == false)
      throw new RuntimeException(s"Some data files ended, not all. Line $lineNr. ")
  }

}

