package sf_crime.combineFiles

import java.io.{File, PrintWriter}

import sf_crime.utils.{FilePaths, HasContext, RunContext}

import scala.annotation.tailrec
import sf_crime._

object CombinedFile  {
  private val linesPerFile = 1000000

  def main(args: Array[String]): Unit = {
    create()
  }

  def create(): Unit = {
    CombinedTrain.create(linesPerFile)
    CombinedTest.create(linesPerFile)
  }
}

object CombinedTrain extends CombineFunctions with ValidateCombiningFiles {
  def context = RunContext.trainContext

  def create(linesPerFile: Int) = {
    val trainFilesToCombine = Seq(
      FilePaths.trainDataAddressMultiple,
      FilePaths.trainDataTime,
      FilePaths.trainDataCoord,
      FilePaths.trainDataPdDistrict
    )
    validateFileCombining(trainFilesToCombine)
    createCombinedFile(trainFilesToCombine, FilePaths.trainCombinedPre, linesPerFile)
  }
}

object CombinedTest extends CombineFunctions with ValidateCombiningFiles {
  def context = RunContext.testContext

  def create(linesPerFile: Int) = {
    val testFilesToCombine = Seq(
      FilePaths.testDataAddressMultiple,
      FilePaths.testDataTime,
      FilePaths.testDataCoord,
      FilePaths.testDataPdDistrict)
    validateFileCombining(testFilesToCombine)
    createCombinedFile(testFilesToCombine, FilePaths.testCombinedPre, linesPerFile)
  }
}
